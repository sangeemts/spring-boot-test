package com.williamslea.fileupload.test;

import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import com.williamslea.fileupload.model.CompanyDetails;
import com.williamslea.fileupload.model.EventType;
import com.williamslea.fileupload.repository.CompanyRepository;
import com.williamslea.fileupload.repository.EventRepository;
import com.williamslea.fileupload.service.FileUploadService;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
@Transactional
public class FileUploadServiceTest {

	@MockBean
	private CompanyRepository companyRepository;

	@MockBean
	private EventRepository eventRepository;

	@InjectMocks
	FileUploadService fileUploadService;

	@Test
	public void uploadToDB() throws IOException {
		List<CompanyDetails> companyList = new ArrayList<>();
		when(companyRepository.saveAll(anyList())).thenReturn(companyList);
		List<EventType> eventList = new ArrayList<>();
		when(eventRepository.saveAll(anyList())).thenReturn(eventList);

		Path path = Paths.get("src/main/resources/Sample.txt");

		String name = "Sample.txt";
		String originalFileName = "Sample.txt";
		String contentType = "text/plain";
		byte[] content = null;
		try {
		    content = Files.readAllBytes(path);
		} catch (final IOException e) {
		}
		MultipartFile multipartFile = new MockMultipartFile(name,
                originalFileName, contentType, content);
		fileUploadService.uploadToDB(multipartFile);
		verify(companyRepository, times(1)).saveAll(anyList());
		verify(eventRepository, times(1)).saveAll(anyList());
	}
}

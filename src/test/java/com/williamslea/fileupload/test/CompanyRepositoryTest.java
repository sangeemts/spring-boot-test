package com.williamslea.fileupload.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import com.williamslea.fileupload.model.CompanyDetails;
import com.williamslea.fileupload.repository.CompanyRepository;

@DataJpaTest
public class CompanyRepositoryTest {

	@Autowired
	CompanyRepository companyRepository;

	@AfterEach
	void tearDown() {
		companyRepository.deleteAll();
	}

	@Test
	void saveCompanyInfo() {
		// given
		String companyName = "ASH RAIL LTD";
		String companyNumber = "11467106";
		String eventType = "C2";
		String eventDate = "01/05/2020";
		CompanyDetails companyDetails = new CompanyDetails(companyName, companyNumber, eventType, eventDate);
		companyRepository.save(companyDetails);

		//when
		long companyRecords = companyRepository.count();

		// then
		assertEquals(1, companyRecords);
	}
}

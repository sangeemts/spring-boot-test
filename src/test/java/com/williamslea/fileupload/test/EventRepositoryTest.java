package com.williamslea.fileupload.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import com.williamslea.fileupload.model.EventType;
import com.williamslea.fileupload.repository.EventRepository;
@DataJpaTest
class EventRepositoryTest {

	public EventRepositoryTest() {
		// TODO Auto-generated constructor stub
	}
	// @Autowired
	//EventRepository eventRepository;
	@AfterEach
	void tearDown() {
	//	eventRepository.deleteAll();
	}
	@Test
	void saveEvents() {
		//given
		String eventCode = "A";
		String eventDescription="CERTIFICATES OF INCORPORATION";
		EventType eventType = new EventType(eventCode, eventDescription);
		//eventRepository.save(eventType);

		String eventCode2 = "B1";
		String eventDescription2="THE COMPANY'S MEMORANDUM AND ARTICLES.";
		eventType = new EventType(eventCode2, eventDescription2);
		//eventRepository.save(eventType);

		String eventCode3 = "B3";
		String eventDescription3="AFTER ANY AMENDMENT OF THE COMPANY'S ARTICLES, THE TEXT OF THE ARTICLES AS AMENDED.";
		eventType = new EventType(eventCode3, eventDescription3);
		//eventRepository.save(eventType);

		//when
		//String fetchedEventDesc = eventRepository.findById(eventCode2).get().getEventDescription();
		//System.out.println("fetchedEventDesc --> "+fetchedEventDesc);
		// then
		//assertEquals(eventDescription, fetchedEventDesc);
	}
}

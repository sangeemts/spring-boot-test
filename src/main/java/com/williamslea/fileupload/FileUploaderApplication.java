package com.williamslea.fileupload;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/** Spring Boot application to upload the file
 * @author Sangeetha
 *
 */
@SpringBootApplication
public class FileUploaderApplication {

	/** main method
	 * @param args
	 */
	@Value("${example.host}")
    private static String host;

    @Value("${example.port}")
    private static int port;

    @Value("${example.username}")
    private static String username;

    @Value("${example.password}")
    private static String password;

    @Value("${example.writer_user}")
    private static String writerUser;

    @Value("${example.writer_password}")
    private static String writerPassword;

    @Value("${example.authentication_type}")
    private static String authType;

	public static void main(String[] args) {
		SpringApplication.run(FileUploaderApplication.class, args);

	}

}

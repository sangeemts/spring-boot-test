package com.williamslea.fileupload.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="eventtypes")
public class EventTypes {

	private List<EventType> eventType;
	
	public EventTypes() {
		super();
	}

	public EventTypes(List<EventType> eventType) {
		super();
		this.eventType = eventType;
	}

	@XmlElement(name="eventtype") 
	public List<EventType> getEventType() {
		return eventType;
	}

	public void setEventType(List<EventType> eventType) {
		this.eventType = eventType;
	}

	
}

package com.williamslea.fileupload.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name="eventtype")
@Entity
public class EventType {

	@Id
	private String eventCode;
	@Lob
	private String eventDescription;

	public EventType(String eventCode, String eventDescription) {
		this.eventCode = eventCode;
		this.eventDescription = eventDescription;
	}
	public EventType() {
		// TODO Auto-generated constructor stub
	}
	
	public String getEventDescription() {
		return eventDescription;
	}
	@XmlElement(name="eventdescription")
	public void setEventDescription(String eventDescription) {
		this.eventDescription = eventDescription;
	}

	public String getEventCode() {
		return eventCode;
	}
	@XmlElement(name="eventcode")
	public void setEventCode(String eventCode) {
		this.eventCode = eventCode;
	}

}

package com.williamslea.fileupload.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="companies")
public class Companies {
	private List<CompanyDetails> companyDetails;

	public Companies() {
		super();
	}

	public Companies(List<CompanyDetails> companyDetails) {
		super();
		this.companyDetails = companyDetails;
	}

	@XmlElement(name="companydetails") 
	public List<CompanyDetails> getCompanyDetails() {
		return companyDetails;
	}

	public void setCompanyDetails(List<CompanyDetails> companyDetails) {
		this.companyDetails = companyDetails;
	}
}

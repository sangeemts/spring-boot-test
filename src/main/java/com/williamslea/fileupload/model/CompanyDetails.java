package com.williamslea.fileupload.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="companydetails")
@Entity
public class CompanyDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long uniqueId;
	public Long getUniqueId() {
		return uniqueId;
	}
	@XmlElement(name="uniqueid")
	public void setUniqueId(Long uniqueId) {
		this.uniqueId = uniqueId;
	}

	public CompanyDetails() {

	}
	public CompanyDetails(String companyName, String companyNumber, String eventType,String eventDate) {
		this.companyName = companyName;
		this.companyNumber = companyNumber;
		this.eventType = eventType;
		this.eventDate = eventDate;
	}
	private String companyName;
	private String companyNumber;

	@Lob
	private String eventType;

	public String getEventType() {
		return eventType;
	}
	@XmlElement(name="eventtype")
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	private String eventDate;


	public String getCompanyName() {
		return companyName;
	}
	@XmlElement(name="companyname")
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getCompanyNumber() {
		return companyNumber;
	}
	@XmlElement(name="companynumber")
	public void setCompanyNumber(String companyNumber) {
		this.companyNumber = companyNumber;
	}
	public String getEventDate() {
		return eventDate;
	}
	@XmlElement(name="eventdate")
	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}


}

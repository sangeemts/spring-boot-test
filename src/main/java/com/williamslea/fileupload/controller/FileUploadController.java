package com.williamslea.fileupload.controller;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.JsonNode;
import com.marklogic.client.DatabaseClient;
import com.marklogic.client.DatabaseClientFactory;
import com.marklogic.client.document.JSONDocumentManager;
import com.marklogic.client.document.TextDocumentManager;
import com.marklogic.client.document.XMLDocumentManager;
import com.marklogic.client.io.DOMHandle;
import com.marklogic.client.io.Format;
import com.marklogic.client.io.InputStreamHandle;
import com.marklogic.client.io.JacksonHandle;
import com.marklogic.client.io.StringHandle;
import com.williamslea.fileupload.service.FileUploadService;

/**
 * Controller class to process get and post requests
 *
 * @author Sangeetha
 *
 */
@Controller
public class FileUploadController {

	@Autowired
	private FileUploadService fileUploadService;

	private static final Logger LOGGER = LoggerFactory.getLogger(FileUploadController.class);
	String host = "localhost";
	int port = 8000;
	String user = "admin";
	String password = "admin";



	/**
	 * This method handles the get request
	 *
	 * @param model Model
	 * @return string which is a html page
	 * @throws IOException
	 */
	@GetMapping("/")
	public String listUploadedFiles(Model model) throws IOException {
	
		return "uploadForm";
	}

	public static void run(String host, int port, String user, String
            password) throws FileNotFoundException {

			//Create the database client
					DatabaseClient client =
							  DatabaseClientFactory.newClient(
							    "localhost", 8000, "Documents",
							    new DatabaseClientFactory.DigestAuthContext(user, password));
					LOGGER.info("Client --> "+client);
 
			//Make a document manager to work with text files.
			XMLDocumentManager XMLdocMgr = client.newXMLDocumentManager();
			LOGGER.info("docMgr --> "+XMLdocMgr);
			//Define a URI value for a document.
			String docId = "/example/sample.xml";
			LOGGER.info("docId --> "+docId);
			//Create a handle to hold string content.
			DOMHandle  handleXML = new DOMHandle();
			StringHandle xmlContent = new StringHandle("<DateOfBirth><Date>23</Date><year>1983</year></DateOfBirth>").withFormat(Format.XML);
			// InputStream input = new FileInputStream("/fun/hello.xml");
			LOGGER.info("handle --> "+xmlContent);
			//Give the handle some content
			//handleXML.set(xmlContent);
			//Write the document to the database with URI from docId
			//and content from handle
			XMLdocMgr.write(docId, xmlContent);
			LOGGER.info("document is written");
			//release the client
			client.release();
			LOGGER.info("client is released");
}

	/**
	 * This method process the post request and file gets uploaded to the Database
	 *
	 * @param multiPartFile      MultipartFile
	 * @param redirectAttributes RedirectAttributes
	 * @return string which is a H2 console
	 * @throws Exception
	 */
	@PostMapping("/")
	public String uploadToDB(@RequestParam("file") MultipartFile multiPartFile) throws Exception {
		 // run(host, port, user, password);
			fileUploadService.uploadToDB(multiPartFile);
			return "redirect:/h2-console";
	}


}

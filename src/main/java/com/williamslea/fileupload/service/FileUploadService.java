package com.williamslea.fileupload.service;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.marklogic.client.DatabaseClient;
import com.marklogic.client.DatabaseClientFactory;
import com.marklogic.client.document.DocumentWriteSet;
import com.marklogic.client.document.XMLDocumentManager;
import com.marklogic.client.io.JAXBHandle;
import com.williamslea.fileupload.model.Companies;
import com.williamslea.fileupload.model.CompanyDetails;
import com.williamslea.fileupload.model.EventType;
import com.williamslea.fileupload.model.EventTypes;
import com.williamslea.fileupload.repository.CompanyRepository;
import com.williamslea.fileupload.repository.EventRepository;

/**
 * This class reads the text file being uploaded from the end point and inserts
 * the records in the database
 *
 * @author Sangeetha
 *
 */
@Service
public class FileUploadService {

	@Autowired
	private EventRepository eventRepository;
	@Autowired
	private CompanyRepository companyRepository;

	private static final Logger LOGGER = LoggerFactory.getLogger(FileUploadService.class);

	@Value("${textfile.startindex}")
	private int startIndex;
	@Value("${eventcode.endindex}")
	private int eventCodeEndIndex;
	@Value("${companyname.endindex}")
	private int companyNameEndIndex;
	@Value("${companynumber.endindex}")
	private int companyNumberEndIndex;
	@Value("${eventtype.endindex}")
	private int eventTypeEndIndex;
	@Value("${eventdate.endindex}")
	private int eventDateEndIndex;
	@Value("${marklogic.host}")
	private String localhost;
	@Value("${marklogic.port}")
	private int port;
	@Value("${marklogic.username}")
	private String username;
	@Value("${marklogic.password}")
	private String password;
	@Value("${marklogic.authentication_type}")
	private String authenticationType;

	@Bean
	public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
		return new PropertySourcesPlaceholderConfigurer();
	}

	/* Pattern to extract the event codes within () */
	Pattern pattern = Pattern.compile("\\((.*?)\\)");

	/**
	 * This method will insert the records into H2 Database
	 *
	 * @param multiFile Text file being uploaded from the end point
	 */
	public void uploadToDB(MultipartFile multiFile) throws IOException {
		LOGGER.info("Going to upload in Database");
		try {
			List<String> eventsFromFile = new ArrayList<>();
			List<EventType> eventTypeList = new ArrayList<>();
			List<CompanyDetails> finalCompanyList = new ArrayList<>();
			int i = 0;
			List<String> companyInfoFromFile = new ArrayList<>();
			BufferedReader reader = new BufferedReader(new InputStreamReader(multiFile.getInputStream()));

			Stream<String> stream = reader.lines();
			boolean isAsterisk[] = { false };
			stream.forEach(k -> {
				if (k.contains("*")) {
					isAsterisk[0] = true;
				} else if (!k.trim().isEmpty() && !k.trim().equals("LOUISE SMYTH")
						&& !k.trim().equals("REGISTRAR OF COMPANIES") && !isAsterisk[0]) {
					eventsFromFile.add(k);
				} else if (!k.trim().isEmpty() && isAsterisk[0] && !k.contains("*")) {
					companyInfoFromFile.add(k);
				}
			});
			// Create the database client
			DatabaseClient client = DatabaseClientFactory.newClient(localhost, port, "Documents",
					new DatabaseClientFactory.DigestAuthContext(username, password));

			eventTypeList = updateEventRecords(i, eventsFromFile, eventTypeList);

			XMLDocumentManager docMgr = client.newXMLDocumentManager();
			DocumentWriteSet writeSet = docMgr.newWriteSet();
			LOGGER.info("Events will be written to ML");

			JAXBContext context = JAXBContext.newInstance(EventTypes.class);
			Marshaller marshallerObj = context.createMarshaller();
			marshallerObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			JAXBHandle<EventTypes> eventHandles = new JAXBHandle<EventTypes>(context);
			EventTypes eventTypes = new EventTypes(eventTypeList);
			marshallerObj.marshal(eventTypes, new FileOutputStream("eventTypes.xml"));
			eventHandles.set(eventTypes);
			writeSet.add("eventTypes.xml", eventHandles);
			eventRepository.saveAll(eventTypeList);
			LOGGER.info("Events are written to ML");

			i = 0;
			finalCompanyList = updateCompanyRecords(i, companyInfoFromFile, finalCompanyList);
			LOGGER.info("Companies will be written to ML");

			JAXBContext compContext = JAXBContext.newInstance(Companies.class);
			Marshaller marshallObj = compContext.createMarshaller();
			marshallObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			JAXBHandle<Companies> companyHandle = new JAXBHandle<Companies>(compContext);
			Companies companies2 = new Companies(finalCompanyList);
			marshallObj.marshal(companies2, new FileOutputStream("companies.xml"));
			companyHandle.set(companies2);
			writeSet.add("companies.xml", companyHandle);
			docMgr.write(writeSet);
			LOGGER.info("Companies are written to ML");
			client.release();
			reader.close();
		} catch (IOException e) {
			LOGGER.error(e.toString());
			e.printStackTrace();
		} catch (Exception e) {
			LOGGER.error(e.toString());
			e.printStackTrace();
		}

	}

	/**
	 * This method retrieves the company details from a line in the text file and
	 * adds the retrieved value to the list.
	 *
	 * @param i                integer to loop through the list
	 * @param companyList      List of strings retrieved from the text file
	 * @param finalCompanyList List to add the retrieved company information
	 *                         containing company name, company number, event
	 *                         description, event date and unique identifier.
	 * @return List of Company with details
	 */
	private List<CompanyDetails> updateCompanyRecords(int i, List<String> companyList,
			List<CompanyDetails> finalCompanyList) {
		/*
		 * Assumed this as a fixed length strings with company name, company number,
		 * event code, event date.
		 */
		/* Index 0 to 47 has company name */
		/* Index 47 to 59 has company number */
		/* Index 59 to 64 has event code */
		/* Index 64 till end of line has event date */

		AtomicInteger count = new AtomicInteger(i);
		companyList.forEach(company -> {
			count.incrementAndGet();
			String companyName = "";
			String companyNumber = "";
			if (company.length() > companyNameEndIndex) {
				companyNumber = company.substring(companyNameEndIndex, companyNumberEndIndex);
			}
			String eventTyp = "";
			String eventDate = "";
			CompanyDetails companyDetails = new CompanyDetails();
			/*
			 * Few lines contain empty company number which implies that the company name is
			 * a continuation of the previous line
			 */
			if (companyNumber.trim().equals("")) {
				String prevLine = companyList.get(count.get() - 2);
				String prevCompName = prevLine.substring(startIndex, companyNameEndIndex);
				/* There are few scenarios where company name is wrapped to third line also */
				if (prevLine.substring(companyNameEndIndex, companyNumberEndIndex).trim().equals("")) {
					String secondPrevLine = companyList.get(count.get() - 3);
					prevCompName = secondPrevLine.substring(startIndex, companyNameEndIndex).trim() + " "
							+ prevCompName.trim() + companyName.trim();
					companyName = prevCompName;
					companyNumber = secondPrevLine.substring(companyNameEndIndex, companyNumberEndIndex);
					eventTyp = secondPrevLine.substring(companyNumberEndIndex, eventTypeEndIndex);
					Matcher matcher = pattern.matcher(eventTyp);
					while (matcher.find()) {
						eventTyp = matcher.group(1);
					}
					eventDate = secondPrevLine.substring(eventTypeEndIndex, eventDateEndIndex);
				} else {
					companyName = company.substring(startIndex, companyNameEndIndex);
					companyName = prevCompName.trim() + " " + companyName.trim();
					companyNumber = prevLine.substring(companyNameEndIndex, companyNumberEndIndex);
					eventTyp = prevLine.substring(companyNumberEndIndex, eventTypeEndIndex);
					Matcher matcher = pattern.matcher(eventTyp);
					while (matcher.find()) {
						eventTyp = matcher.group(1);
					}
					eventDate = prevLine.substring(eventTypeEndIndex, eventDateEndIndex);
				}

			} else {
				companyDetails = new CompanyDetails();
				companyName = company.substring(startIndex, companyNameEndIndex);
				companyNumber = company.substring(companyNameEndIndex, companyNumberEndIndex);
				eventTyp = company.substring(companyNumberEndIndex, eventTypeEndIndex);
				Matcher matcher = pattern.matcher(eventTyp);
				while (matcher.find()) {
					eventTyp = matcher.group(1);
				}
				eventDate = company.substring(eventTypeEndIndex, eventDateEndIndex);
			}
			companyDetails.setCompanyName(companyName.trim());
			companyDetails.setCompanyNumber(companyNumber.trim());
			String eventDescription = "";
			/*
			 * To update the company table with event description equivalent to event code
			 */
			try {
				eventDescription = eventRepository.findById(eventTyp).get().getEventDescription();
			} catch (Exception e) {
				LOGGER.warn("Company Number " + companyNumber.trim() + " doesn't have event code");
			}
			companyDetails.setEventType(eventDescription);
			companyDetails.setEventDate(eventDate.trim());
			finalCompanyList.add(companyDetails);
		});

		return finalCompanyList;
	}

	/**
	 * This method retrieves the event code and event description from a line in the
	 * text file and adds the retrieved value to the list.
	 *
	 * @param i              integer to loop through the list
	 * @param eventsFromFile List of strings retrieved from the text file
	 * @param eventTypeList  List to add the retrieved events with event code and
	 *                       description
	 * @return List of EventType
	 */
	private List<EventType> updateEventRecords(int i, List<String> eventsFromFile, List<EventType> eventTypeList) {
		/*
		 * Assumed this as a fixed length strings with event code and event description.
		 */
		/* Index 0 to 27 has event code and is referred from application properties */
		/* Index 27 to end of line has event description */
		AtomicInteger count = new AtomicInteger(i);
		eventsFromFile.forEach(lineNow -> {
			count.incrementAndGet();
			String eventCode = "";
			String eventDescription = "";
			eventCode = lineNow.substring(startIndex, eventCodeEndIndex);
			Matcher matcher = pattern.matcher(eventCode);
			EventType eventObject = new EventType();
			while (matcher.find()) {
				eventCode = matcher.group(1);
			}
			eventDescription = lineNow.substring(eventCodeEndIndex, lineNow.length());
			boolean hasNextEventCode = false;
			/*
			 * Few lines contain empty event code which implies that the company name is a
			 * continuation of the previous line
			 */
			if (eventCode.trim().equals("")) {
				hasNextEventCode = true;
				String prevLine = eventsFromFile.get(count.get() - 2);
				eventCode = prevLine.substring(startIndex, eventCodeEndIndex);
				String secondPrevDesc = lineNow.substring(eventCodeEndIndex, lineNow.length());
				/* There are few scenarios where event name is wrapped to third line also */
				if (eventCode.trim().equals("")) {
					String thirdPrevLine = eventsFromFile.get(count.get() - 3);
					String thirdPrevDesc = thirdPrevLine.substring(eventCodeEndIndex, thirdPrevLine.length());
					eventCode = thirdPrevLine.substring(startIndex, eventCodeEndIndex);
					Matcher mat = pattern.matcher(eventCode);
					while (mat.find()) {
						eventCode = mat.group(1);
					}
					eventDescription = thirdPrevDesc.trim() + " " + prevLine.trim() + " " + secondPrevDesc.trim();
				} else {
					Matcher mat = pattern.matcher(eventCode);
					while (mat.find()) {
						eventCode = mat.group(1);
					}
					prevLine = prevLine.substring(eventCodeEndIndex, prevLine.length());
					eventDescription = prevLine + " " + eventDescription;
				}
			} else {
				eventObject = new EventType();
				if (hasNextEventCode) {
					eventDescription = eventDescription.trim() + eventsFromFile.get(count.get() - 1).trim();
				} else {
					eventDescription = eventDescription.trim();
				}
			}
			eventObject.setEventCode(eventCode.trim());
			eventObject.setEventDescription(eventDescription.trim());
			eventTypeList.add(eventObject);

		});
		return eventTypeList;
	}
}

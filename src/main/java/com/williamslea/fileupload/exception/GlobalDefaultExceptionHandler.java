package com.williamslea.fileupload.exception;

import java.io.IOException;
import java.nio.file.NoSuchFileException;
import java.util.NoSuchElementException;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalDefaultExceptionHandler extends Exception{

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public static final String ERROR_VIEW = "customerror";

	private static final Logger LOGGER = LoggerFactory.getLogger(GlobalDefaultExceptionHandler.class);

	@ExceptionHandler(value = Exception.class)
	  public ResponseEntity<String> defaultErrorHandler(HttpServletRequest req, Exception e) throws Exception {
		LOGGER.info("Exception -->"+ e.toString());
		return new ResponseEntity<>("Exception has occured, please contact administrator", HttpStatus.BAD_REQUEST);
	}
	@ExceptionHandler(value = RuntimeException.class)
	  public ResponseEntity<String> defaultErrorHandler(HttpServletRequest req, RuntimeException e) throws Exception {
		return new ResponseEntity<>("RuntimeException has occured, please contact administrator", HttpStatus.INTERNAL_SERVER_ERROR);
	}
	@ExceptionHandler(value = NoSuchElementException.class)
	  public ResponseEntity<String> defaultErrorHandler(HttpServletRequest req, NoSuchElementException e) throws Exception {
		return new ResponseEntity<>("NoSuchElementException has occured, please contact administrator", HttpStatus.NOT_FOUND);
	}
	@ExceptionHandler(value = StringIndexOutOfBoundsException.class)
	  public ResponseEntity<String> defaultErrorHandler(HttpServletRequest req, StringIndexOutOfBoundsException e) throws Exception {
		return new ResponseEntity<>("StringIndexOutOfBoundsException has occured, please contact administrator", HttpStatus.INTERNAL_SERVER_ERROR);
	}
	@ExceptionHandler(value = IOException.class)
	  public ResponseEntity<String> defaultErrorHandler(HttpServletRequest req, IOException e) throws Exception {
		return new ResponseEntity<>("IOException has occured, please contact administrator", HttpStatus.INTERNAL_SERVER_ERROR);
	}
	@ExceptionHandler(value = NoSuchFileException.class)
	  public ResponseEntity<String> defaultErrorHandler(HttpServletRequest req, NoSuchFileException e) throws Exception {
		return new ResponseEntity<>("NoSuchFileException has occured, please contact administrator", HttpStatus.INTERNAL_SERVER_ERROR);
	}
	@ExceptionHandler(value = IllegalStateException.class)
	  public ResponseEntity<String> defaultErrorHandler(HttpServletRequest req, IllegalStateException e) throws Exception {
		return new ResponseEntity<>("IllegalStateException has occured, please contact administrator", HttpStatus.INTERNAL_SERVER_ERROR);
	}
}

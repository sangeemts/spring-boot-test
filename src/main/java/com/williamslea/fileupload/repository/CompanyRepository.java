package com.williamslea.fileupload.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.williamslea.fileupload.model.CompanyDetails;

/**
 * @author Sangeetha
 *
 */
@Repository
public interface CompanyRepository extends CrudRepository<CompanyDetails, String>{

}

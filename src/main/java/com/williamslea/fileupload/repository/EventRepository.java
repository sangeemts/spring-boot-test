package com.williamslea.fileupload.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.williamslea.fileupload.model.EventType;

/**
 * @author Sangeetha
 *
 */
@Repository
public interface EventRepository extends CrudRepository<EventType, String>{



}
